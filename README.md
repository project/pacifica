# Pacifica Module

This module interfaces with the [Pacifica](https://github.com/pacifica)
software suite to facilitate the building of interfaces on top of Pacifica
services.
